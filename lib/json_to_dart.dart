import 'dart:convert';

String jsonToDart(
  String json, {
  String className = 'MyClass',
  String indent = '  ',
}) {
  final Map<String, dynamic> map = jsonDecode(json);
  var dartCode = 'class $className {\n';
  var missingClasses = '';

  // Define members
  map.forEach((key, value) {
    dartCode += '${indent}final ';
    if (value is Map) {
      // Recursive define missing classes here
      final missingClassName = key.camelCased.capitalized;
      dartCode += missingClassName;
      missingClasses +=
          '\n\n${jsonToDart(jsonEncode(value), className: missingClassName)}';
    } else if (value is List) {
      final list = value;
      var listType = 'Object';
      if (list.isNotEmpty) {
        listType = list.first.runtimeType.toString();
        if (list.any((item) => item.runtimeType.toString() != listType)) {
          listType = 'Object';
        }
      }
      if (listType.contains('Map')) {
        final missingClassName = (key.camelCased.endsWith('s')
                ? key.camelCased.substring(0, key.length - 1)
                : key.camelCased)
            .capitalized;
        missingClasses +=
            '\n\n${jsonToDart(jsonEncode(list.first), className: missingClassName)}';
        listType = missingClassName;
      }
      dartCode += 'List<$listType>';
    } else {
      dartCode += '${value.runtimeType.toString()}';
    }
    if (key.endsWith('?')) dartCode += '?';
    dartCode += ' ${key.camelCased};\n';
  });

  // Constructor
  dartCode += '\n${indent}const $className({\n';
  map.forEach((key, value) {
    dartCode += indent * 2;
    if (!key.endsWith('?')) dartCode += 'required ';
    dartCode += 'this.${key.camelCased},\n';
  });
  dartCode += '$indent});\n';

  // fromJson
  dartCode +=
      '\n${indent}factory $className.fromJson(Map<String, dynamic> json) => $className(\n';
  map.forEach((key, value) {
    final nullable = key.endsWith('?');
    dartCode += '${indent * 2}${key.camelCased}: ';
    if (value is Map) {
      final missingClassName = key.camelCased.capitalized;
      if (nullable) {
        dartCode += "json['${key.removeNullableAnnotation}'] == null ? null : ";
      }
      dartCode +=
          "$missingClassName.fromJson(json['${key.removeNullableAnnotation}'])";
    } else if (value is List) {
      final list = value;
      var listType = 'Object';
      if (list.isNotEmpty) {
        listType = list.first.runtimeType.toString();
        if (list.any((item) => item.runtimeType.toString() != listType)) {
          listType = 'Object';
        }
      }
      if (listType.contains('Map')) {
        final missingClassName = (key.camelCased.endsWith('s')
                ? key.camelCased.substring(0, key.length - 1)
                : key.camelCased)
            .capitalized;
        listType = missingClassName;
        if (nullable) {
          dartCode +=
              "json['${key.removeNullableAnnotation}'] == null ? null : ";
        }
        dartCode +=
            "(json['${key.removeNullableAnnotation}'] as List).map((i) => $listType.fromJson(i)).toList()";
      } else {
        if (nullable) {
          dartCode +=
              "json['${key.removeNullableAnnotation}'] == null ? null : ";
        }
        dartCode +=
            "List<$listType>.from(json['${key.removeNullableAnnotation}'])";
      }
    } else {
      dartCode += "json['${key.removeNullableAnnotation}']";
    }
    dartCode += ',\n';
  });
  dartCode += '$indent);\n';

  // toJson
  dartCode += '\n${indent}Map<String, dynamic> toJson() => {\n';
  map.forEach((key, value) {
    final nullable = key.endsWith('?');
    dartCode += indent * 2;
    if (nullable) {
      dartCode +=
          "if (${key.camelCased} != null) '${key.removeNullableAnnotation}': ";
    } else {
      dartCode += "'${key.removeNullableAnnotation}': ";
    }
    if (value is Map) {
      dartCode += '${key.camelCased}${nullable ? '?' : ''}.toJson()';
    } else if (value is List) {
      final list = value;
      var listType = 'Object';
      if (list.isNotEmpty) {
        listType = list.first.runtimeType.toString();
        if (list.any((item) => item.runtimeType.toString() != listType)) {
          listType = 'Object';
        }
      }
      if (listType.contains('Map')) {
        final missingClassName = (key.camelCased.endsWith('s')
                ? key.camelCased.substring(0, key.length - 1)
                : key.camelCased)
            .capitalized;
        listType = missingClassName;
        dartCode += '${key.camelCased}.map((i) => i.toJson()).toList()';
      } else {
        dartCode += 'List<$listType>.from(${key.camelCased})';
      }
    } else {
      dartCode += key.camelCased;
    }
    dartCode += ',\n';
  });
  dartCode += '$indent};';

  // End class and add missing classes
  dartCode += '\n}\n';
  dartCode += missingClasses;
  return dartCode;
}

extension on String {
  String get capitalized => '${this[0].toUpperCase()}${substring(1)}';
  String get uncapitalized => '${this[0].toLowerCase()}${substring(1)}';
  String get removeNullableAnnotation => replaceFirst('?', '');
  String get camelCased => split('_')
      .map((s) => s.capitalized)
      .join('')
      .uncapitalized
      .removeNullableAnnotation;
}
