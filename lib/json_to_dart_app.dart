import 'dart:html';

import 'package:json_to_dart_web/json_to_dart.dart';
import 'package:rad/rad.dart';
import 'package:rad/widgets_html.dart';

class JsonToDartApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => JsonToDartAppState();
}

class JsonToDartAppState extends State<JsonToDartApp> {
  String? get classNameTextFieldValue =>
      (document.getElementById('classNameInputField') as InputElement?)?.value;
  String? get jsonTextAreaValue =>
      (document.getElementById('jsonTextArea') as TextAreaElement?)?.value;

  TextAreaElement? get dartOutputTextArea =>
      (document.getElementById('dartTextArea') as TextAreaElement?);

  String _dartText = '';
  String _errorText = '';

  void _convertAction([_]) {
    print('Start converting...');
    final jsonText = jsonTextAreaValue;
    if (jsonText == null) return;
    try {
      final dartText = jsonToDart(
        jsonText,
        className: classNameTextFieldValue ?? 'JsonClass',
      );
      setState(() {
        _errorText = '';
        _dartText = dartText;
      });
      dartOutputTextArea?.value = dartText;
    } catch (e) {
      setState(() {
        _errorText = e.toString();
      });
    }
  }

  void _copyToClipboardAction([_]) async {
    dartOutputTextArea?.select();
    dartOutputTextArea?.setSelectionRange(
        0, dartOutputTextArea?.value?.length ?? 0);
    await window.navigator.clipboard
        ?.writeText(dartOutputTextArea?.value ?? '');
    window.alert('Copied to clipboard');
  }

  @override
  Widget build(context) => Division(
        classAttribute: 'container mx-auto px-4 max-w-screen-lg',
        children: [
          Heading1(
            innerText: 'JSON to Dart converter',
            classAttribute: 'text-xl',
          ),
          Paragraph(
            classAttribute: 'text-gray-400',
            innerText:
                'Super easy JSON to Dart converter with null safety! To make a field nullable, just suffix the key with `?`.',
          ),
          InputText(
            id: 'classNameInputField',
            placeholder: 'Class name',
            classAttribute: 'w-full border mt-4 p-2',
          ),
          TextArea(
            id: 'jsonTextArea',
            placeholder: 'Enter some JSON...',
            maxLength: 9999,
            classAttribute: 'w-full h-48 border mt-4 p-2',
          ),
          Paragraph(
            innerText: _errorText,
            classAttribute: _errorText.isEmpty ? '' : 'text-red-700',
          ),
          Button(
            innerText: 'Convert to Dart',
            classAttribute:
                'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded mt-4 mr-4',
            onClick: _convertAction,
          ),
          if (_dartText.isNotEmpty)
            Button(
              innerText: 'Copy to clipboard',
              classAttribute:
                  'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded mt-4',
              onClick: _copyToClipboardAction,
            ),
          TextArea(
            id: 'dartTextArea',
            placeholder: 'Dart output',
            readOnly: true,
            maxLength: 9999,
            classAttribute: 'w-full h-96 border mt-4 p-2',
          ),
          LinkList(),
        ],
      );
}

class LinkList extends StatelessWidget {
  @override
  Widget build(context) {
    return Span(children: [
      Anchor(
        classAttribute: 'text-blue-400 hover:text-blue-700 underline mt-4',
        href: 'https://krillefear.gitlab.io',
        innerText: 'Made by me',
      ),
      Span(innerText: ' - '),
      Anchor(
        classAttribute: 'text-blue-400 hover:text-blue-700 underline mt-4',
        href: 'https://gitlab.com/KrilleFear/json-to-dart-converter',
        innerText: 'Source code',
      ),
      Span(innerText: ' - '),
      Anchor(
        classAttribute: 'text-blue-400 hover:text-blue-700 underline mt-4',
        href: 'https://gitlab.com/KrilleFear/json-to-dart-converter/issues',
        innerText: 'Issues',
      ),
      Anchor(
        classAttribute: 'text-blue-400 hover:text-blue-700 underline mt-4',
        href:
            'https://gitlab.com/KrilleFear/json-to-dart-converter/-/blob/main/LICENSE',
        innerText: 'License',
      ),
    ]);
  }
}
