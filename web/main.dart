import 'package:json_to_dart_web/json_to_dart_app.dart';
import 'package:rad/rad.dart';

void main() => runApp(app: JsonToDartApp(), targetId: 'output');
